<?php

namespace App\Services;

use App\Models\Response;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Facades\DB;

class NasaApi
{
    protected $client;

    protected $uri = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=';

    protected $record;

    protected $date;

    protected $url;

    protected $title;

    protected $media_type;


    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function addRecord()
    {
        $response = new Response();
        $response->date = $this->date;
        $response->url = $this->url;
        $response->title = $this->title;
        $response->media_type = $this->media_type;
        $response->save();
    }


    public function getApod($date)
    {

        $response = $this->client->request('GET', $this->uri . $date);

        $result = json_decode($response->getBody());

       // print_r($result);

        $this->record = Db::table('responses')->where('date', $date)->first();

        if ($this->record) {
            $this->date = $this->record->date;
            $this->url = $this->record->url;
            $this->title = $this->record->title;
            $this->media_type = $this->record->media_type;
        } else {
            $this->date = $result->date;
            $this->url = $result->url;
            $this->title = $result->title;
            $this->media_type = $result->media_type;
            $this->addRecord();
        }


        if ($this->media_type == 'image'){
            return view('pages.home', [
                'url_image' => $this->url,
                'title' => $this->title
            ]);
        } elseif ($this->media_type == 'video') {
            return view('pages.video', [
                'url_video' => $this->url,
                'title' => $this->title
            ]);
        }

    }

}
