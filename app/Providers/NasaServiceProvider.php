<?php


namespace App\Providers;


use App\Services\NasaApi;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class NasaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('client', function($app){
            return new Client();
        });

        $this->app->bind('NasaApi', function($app){
            return new NasaApi(app()->get('client'));
        });
    }

}
