<?php


namespace App\Http\Controllers;


use App\Models\Response;
use Illuminate\Http\Request;

class NasaController
{

    public function date()
    {
        return view('pages.date');
    }

    public function nasa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $date = $_POST['date'];
        }



        $service = app()->get('NasaApi');
        $result = $service->getApod($date);
        return $result;

    }

}
