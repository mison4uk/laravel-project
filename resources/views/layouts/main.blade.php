<html>
<head>
    <title>@yield('title')</title>
</head>
<body>
<header class="header">
    <h1>Astronomy Picture of the Day</h1>
</header>

<div class="container">

    <div class="date">
        @yield('date')
    </div>

    <br>

    <div class="content">
        @yield('content')
    </div>

</div>

<footer class="footer">
    @yield('footer')
</footer>
</body>
</html>
