@extends('layouts.main')

@section('content')
    <h3>{{ $title }}</h3>
    <iframe width="560" height="315" src="{{ $url_video }}" frameborder="0" allowfullscreen>
    </iframe>

@endsection

@section('title')
    Video page
@endsection

@section('footer')
    <h3>Authors & editors: Robert Nemiroff (MTU) & Jerry Bonnell (UMCP)</h3>
    <h3>   NASA Official: Phillip Newman Specific rights apply.</h3>
    <h3>      NASA Web Privacy Policy and Important Notices</h3>
    <h3>            A service of: ASD at NASA / GSFC</h3>
    <h3>                   & Michigan Tech. U.</h3>
@endsection
